(local objects {})

(lambda objects.new-collider [type points]
  ;; FIX: Better naming
  (local thingie {})
  (set thingie.__index thingie)
  (setmetatable thingie _G.bf.Collider)
  (local collider (_G.world:newCollider type points))
  (setmetatable collider thingie)
  collider)

(lambda objects.despawn-all [self]
  "Despawns all objects indexed as a number. Special entries like `objects.player` are untouched."
  (for [i 1 (length self)]
    (: (. self i) :destroy)
    (tset self i nil))
  )

(lambda objects.out-of-bounds? [collider]
  (local points [(collider:getWorldPoints (collider:getPoints))])
  ;; TODO: Create a macro for this (`all` and `any` like in Rust)
  (for [i 1 (length points) 2]
    (let [x (. points i)]
      (if (> x 0)
          (lua "return false"))))
  true)

(lambda objects.spawn-ground [self]
  (set self.ground (objects.new-collider :Polygon [0 _G.ground-height
                                                   _G.canvas.width _G.ground-height
                                                   _G.canvas.width _G.canvas.height
                                                   0 _G.canvas.height]))
  (self.ground:setDrawOrder -1)
  (self.ground:setType :static)
  (self.ground.fixture:setUserData {:type :Ground})
  (fn self.ground.draw []
    (_G.assets.ground:draw 0 _G.ground-height))
  self.ground)

(lambda objects.spawn-player [self]
  (assert (= _G.world.player nil))
  (local player-shape [100 140 4])
  (local player (objects.new-collider :Circle player-shape))
  (set player.initial-position [(table.unpack player-shape 1 2)])
  (set player.score 0)
  (set player.jump-force 350)
  (set player.on-ground? false)
  (set player.anim-handle nil)
  (player:setDrawOrder 6)
  (player:setFixedRotation true)
  (player:setMass 1)
  (player:setGravityScale 12)
  (player:setLinearDamping 2)
  (player:setFriction 0)
  (lambda player.reset [self]
    (set self.score 0)
    (set self.on-ground? false)
    (self:setPosition (table.unpack self.initial-position))
    (self:setLinearVelocity 0 0)
    (self:setAngle 0))
  (lambda player.jump [self]
    (: (love.audio.newSource _G.assets.audio.wing :static) :play)
    (when self.anim-handle
      (_G.Timer.cancel self.anim-handle)
      (let [{: x : y : w : h : quad} _G.assets.atlas.sprites.player]
        (quad:setViewport x y w h)))
    (set self.anim-handle (_G.Timer.script (lambda [wait]
                                             (let [{: x : y : w : h : quad} _G.assets.atlas.sprites.player
                                                   delay .1]
                                               (quad:setViewport (+ x w 1) y w h)
                                               (wait delay)
                                               (quad:setViewport (+ x (* w 2) 2) y w h)
                                               (wait delay)
                                               (quad:setViewport x y w h)
                                               ))))
    (self:setLinearVelocity 0 (- self.jump-force)))
  (lambda player.above-ceiling? [self]
    (let [(_ y) (self:getPosition)
          radius (self.shape:getRadius)]
      (< y (+ (- radius) 1))))
  (fn player.draw []
    (let [(x y) (player:getPosition)]
      (_G.assets.atlas:draw-player x y)))
  (fn player.enter [self other]
    (let [current-state (_G.Gamestate.current)
          playing-state (require :lib/state/playing)
          game-over-state (require :lib/state/game-over)]
      (when (= other.type :Ground)
        (set self.on-ground? true))
      (match current-state
        playing-state (do
                        (when (= other.type :Obstacle-Pass-Sensor)
                          (: (love.audio.newSource _G.assets.audio.point :static) :play)
                          (set self.score (+ self.score 1)))
                        (when (or (= other.type :Tube) (= other.type :Ground))
                          (_G.Gamestate.switch game-over-state))))))
  (fn player.exit [self other]
    (when (= other.type :Ground)
      (set self.on-ground? false)))
  (set self.player player)
  player)

(lambda objects.spawn-tube [self x h ?upside-down?]
  "Spawns a singular tube. Keep in mind that x is in the center of the tube."
  (local tube (objects.new-collider :Rectangle [x (if ?upside-down?
                                                      (/ h 2)
                                                      (- _G.ground-height (/ h 2)))
                                                _G.assets.atlas.sprites.tube.w h]))
  (tube:setDrawOrder -1)
  (tube:setType :kinematic)
  (tube:setSensor true)
  (tube.fixture:setUserData {:type :Tube})
  (fn tube.draw [self]
    (local (x y) (self:getPosition))
    (_G.assets.atlas:draw-tube x y h ?upside-down?))
  (table.insert self tube)
  tube)

(lambda objects.spawn-obstacle [self x y h]
  "Spawns 2 tubes at 'x with a gap defined using 'y and 'h.
  Keep in mind that 'y is not centered."
  (local min-offset 18)
  (local min-y min-offset)
  (local max-y (- _G.ground-height h min-offset))
  (assert (and (>= y min-y)
               (<= y max-y))
          (.. "Y must be in range " min-y "..." max-y ", got y=" y " h=" h))
  (local top-tube (self:spawn-tube x y true))
  (local bottom-tube (self:spawn-tube x (- _G.ground-height h y)))
  (local obstacle-sensor (objects.new-collider :Rectangle [x (+ y (/ h 2))
                                                           2 h]))
  (obstacle-sensor:setType :kinematic)
  (obstacle-sensor:setSensor true)
  (obstacle-sensor.fixture:setUserData {:type :Obstacle-Pass-Sensor})
  (fn obstacle-sensor.draw [])
  (table.insert objects obstacle-sensor)
  (values top-tube bottom-tube obstacle-sensor))

objects
