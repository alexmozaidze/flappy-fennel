(lambda quad-scroll [quad x y]
  (let [(old-x old-y w h) (quad:getViewport)]
    (quad:setViewport (+ old-x x) (+ old-y y) w h)))

(lambda draw-axis-guides [width height]
  (let [old-color [(love.graphics.getColor)]
        draw-vertical #(love.graphics.line (/ width 2) 0
                                           (/ width 2) height)
        draw-horizontal #(love.graphics.line 0               (/ height 2)
                                             width (/ height 2))]
    (love.graphics.setColor 1 0 0)
    (draw-horizontal)
    (love.graphics.setColor 0 1 0)
    (draw-vertical)
    (love.graphics.setColor old-color)))

{: quad-scroll : draw-axis-guides}
