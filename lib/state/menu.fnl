(local menu {})

(fn menu.enter []
  (_G.objects:despawn-all)
  (_G.objects.player:reset))

(fn menu.draw []
  (_G.rs.start)
  (_G.assets.atlas:draw-title (/ _G.rs.gameWidth 2) 40 1 true)
  (_G.rs.stop))

(fn menu.keypressed [_ key]
  (match key
    :space (_G.Gamestate.switch (require :lib/state/playing))))

menu
