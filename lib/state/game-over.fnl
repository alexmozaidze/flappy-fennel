(local game-over {})

(fn game-over.enter []
  (: (love.audio.newSource _G.assets.audio.hit :static) :play)
  (: (love.audio.newSource _G.assets.audio.die :static) :play)
  (set game-over.can-restart? false)
  (_G.Timer.after .3 #(set game-over.can-restart? true))
  (each [_ object (ipairs _G.objects)]
    (object:setLinearVelocity 0 0))
  (let [player _G.objects.player
        (_ y-velocity) (player:getLinearVelocity)]
    (when (< y-velocity 0)
      (player:setLinearVelocity 0 (* y-velocity .5))))
  )

(fn game-over.update [_ dt]
  (_G.world:update dt)
  (let [player _G.objects.player
        (_ y-velocity) (player:getLinearVelocity)
        new-angle (/ y-velocity player.jump-force)]
    (when (not player.on-ground?)
      (player:setAngle new-angle))))

(fn game-over.draw []
  (_G.rs.start)
  (_G.assets.atlas:draw-game-over (/ _G.rs.gameWidth 2) 40)
  (_G.rs.stop))

(fn game-over.keypressed [_ key]
  (match key
    :space (when game-over.can-restart?
             (_G.Gamestate.switch (require :lib/state/menu)))))

game-over
