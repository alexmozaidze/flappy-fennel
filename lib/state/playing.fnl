(local playing {})

(fn playing.init []
  (set playing.timer (_G.Timer.new))
  (lambda spawn-obstacle-at-random-height []
    (let [x (+ _G.canvas.width 40)
          y (love.math.random 18 (- _G.ground-height 65 18))
          h 65]
      (_G.objects:spawn-obstacle x y h)))
  (let [spawn-time 1.8]
    (set playing.spawn-timer (playing.timer:every spawn-time spawn-obstacle-at-random-height)))
  (lambda despawn-out-of-bounds []
    (each [i object (ipairs _G.objects)]
      (when (_G.objects.out-of-bounds? object)
        (object:destroy)
        (table.remove _G.objects i))))
  (set playing.despawn-timer (playing.timer:every 10 despawn-out-of-bounds)))

(fn playing.enter []
  (_G.objects.player:jump))

(fn playing.update [_ dt]
  (_G.assets.background:scroll (* 20 dt) 0)
  (_G.assets.ground:scroll (* 80 dt) 0)
  (let [player _G.objects.player
        (_ y-velocity) (player:getLinearVelocity)]
    (player:setAngle (/ y-velocity player.jump-force)))
  (each [_ object (ipairs _G.objects)]
    (object:setLinearVelocity -80 0))
  (playing.timer:update dt)
  (_G.world:update dt)
  (when (_G.objects.player:above-ceiling?)
    (_G.Gamestate.switch (require :lib/state/game-over))))

(fn playing.draw []
  (_G.rs.start)
  (when _G.debug-mode
    (love.graphics.printf (.. "Objects: " (length _G.objects)) 0 75 _G.canvas.width :center))
  (love.graphics.printf _G.objects.player.score 0 45 _G.canvas.width :center)
  (_G.rs.stop))

(fn playing.keypressed [_ key]
  (match key
    :space (_G.objects.player:jump)))

playing
