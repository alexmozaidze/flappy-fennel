(fn love.handlers.stdin [line]
  ;; evaluate lines read from stdin as fennel code
  (let [(ok val) (pcall _G.fennel.eval line)]
    (print (if ok (_G.fennel.view val) val))))

(: (love.thread.newThread "require('love.event')
                            while 1 do love.event.push('stdin', io.read('*line')) end") :start)
