(local {: quad-scroll} (require :lib/utils))

(local assets {})

;; Atlas
(set assets.atlas {})
(set assets.atlas.image (love.graphics.newImage :assets/atlas.png))
(assets.atlas.image:setWrap :clamp)
(set assets.atlas.gap 1)
(set assets.atlas.sprites {:player (let [x 1  y 1
                                         w 17 h 12
                                         keyframes 3
                                         variants 3
                                         quad (love.graphics.newQuad x y w h (assets.atlas.image:getDimensions))
                                         ]
                                     {: x : y : w : h : keyframes : variants : quad})
                           :tube (let [x 1  y 238
                                       w 26 h 18
                                       variants 2
                                       quad (love.graphics.newQuad x y w h (assets.atlas.image:getDimensions))]
                                   {: x : y : w : h : variants : quad})
                           :tap-to-play {:x 93 :y 85
                                         :w 57 :h 49}
                           :title (let [x 151 y 85
                                        w 89  h 24
                                        quad (love.graphics.newQuad x y w h (assets.atlas.image:getDimensions))]
                                    {: x : y : w : h : quad})
                           :game-over (let [x 148 y 1
                                            w 96  h 21
                                            quad (love.graphics.newQuad x y w h (assets.atlas.image:getDimensions))]
                                        {: x : y : w : h : quad})
                           })

(lambda assets.atlas.draw-player [self x y ?scale]
  (local player self.sprites.player)
  (local scale (or ?scale 1))
  (local x (+ (- x (* player.w scale .5)) (* player.w .5)))
  (local y (+ (- y (* player.h scale .5)) (* player.h .5)))
  (love.graphics.draw
    self.image
    player.quad
    x y
    (_G.objects.player:getAngle)
    scale scale
    (* player.w .5 scale)
    (* player.h .5 scale)))

(lambda assets.atlas.draw-tube [self x y h ?upside-down?]
  (local x (- x (/ self.sprites.tube.w 2)))
  (local y (- y (/ h 2)))
  (local (vx vy vw _vh) (self.sprites.tube.quad:getViewport))
  (self.sprites.tube.quad:setViewport vx vy vw h)
  (love.graphics.draw
    self.image
    self.sprites.tube.quad
    x y
    (if ?upside-down? (math.rad 180) 0)
    1 1
    (if ?upside-down?
        (values vw h)
        (values 0 0))))

(lambda assets.atlas.draw-title [self x y ?scale ?center?]
  (local scale (or ?scale 1))
  (local (x y) (let [(_x _y w h) (self.sprites.title.quad:getViewport) ]
                 (if ?center?
                     (values (- x (/ (* w scale) 2)) (- y (/ (* h scale) 2)))
                     (values x y))))
  (love.graphics.draw
    self.image
    self.sprites.title.quad
    x y
    0
    scale))

(lambda assets.atlas.draw-game-over [self x y]
  (local (x y) (let [(_x _y w h) (self.sprites.game-over.quad:getViewport) ]
                 (values (- x (/ w 2)) (- y (/ h 2)))))
  (love.graphics.draw
    self.image
    self.sprites.game-over.quad
    x y))

;; Background
(set assets.background {})
(set assets.background.day (love.graphics.newImage :assets/background-day.png))
(assets.background.day:setWrap :repeat :clamp)

(let [x 0
      y -140
      (w h) (love.graphics.getDimensions)
      (sw sh) (assets.background.day:getDimensions)]
  (set assets.background.quad (love.graphics.newQuad x y w h sw sh)))

(lambda assets.background.scroll [self x y]
  (quad-scroll self.quad x y))
(lambda assets.background.draw [self ...]
  (love.graphics.draw self.day self.quad ...))

;; Ground
(set assets.ground {})
(set assets.ground.image (love.graphics.newImage :assets/ground.png))
(assets.ground.image:setWrap :repeat :clamp)

(let [x 0
      y 0
      (w h) (love.graphics.getDimensions)
      (sw sh) (assets.ground.image:getDimensions)]
  (set assets.ground.quad (love.graphics.newQuad x y w h sw sh)))

(lambda assets.ground.scroll [self x y]
  (quad-scroll self.quad x y))
(lambda assets.ground.draw [self ...]
  (love.graphics.draw self.image self.quad ...))


(set assets.audio {})
(set assets.audio.die (love.sound.newSoundData :lib/flappy-bird-assets/audio/die.wav))
(set assets.audio.hit (love.sound.newSoundData :lib/flappy-bird-assets/audio/hit.wav))
(set assets.audio.wing (love.sound.newSoundData :lib/flappy-bird-assets/audio/wing.wav))
(set assets.audio.point (love.sound.newSoundData :lib/flappy-bird-assets/audio/point.wav))

assets
