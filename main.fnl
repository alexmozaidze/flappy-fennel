(require :lib/annoying-but-kinda-required-stdio)
(local {: draw-axis-guides} (require :lib/utils))
(set _G.rs (require :lib/resolution_solution/resolution_solution))
(love.graphics.setBackgroundColor .8 .8 .8)
(love.graphics.setDefaultFilter :nearest)
(set _G.assets (require :lib/assets))

(_G.rs.init {:width _G.canvas.width
             :height _G.canvas.height
             :mode 1})

(_G.rs.setMode
  _G.screen.width
  _G.screen.height
  {:resizable true})

(love.window.setTitle "Flappy Fennel")
(love.window.setIcon (love.image.newImageData "lib/flappy-bird-assets/sprites/yellowbird-downflap.png"))

(set _G.love.resize #(_G.rs.resize $1 $2))

(love.graphics.setNewFont 12 :mono)

(love.physics.setMeter 100)

(set _G.Timer (require :lib/hump/timer))
(set _G.Gamestate (require :lib/hump/gamestate))
(set _G.bf (require :lib/breezefield))
(set _G.world (_G.bf.newWorld 0 90.81 true))
(set _G.objects (require :lib/objects))
(set _G.draw-order (require :lib/Grove/grove/draworder))

(_G.objects:spawn-ground)
(_G.objects:spawn-player)

(_G.Gamestate.switch (require :lib/state/menu))

(fn love.draw []
  (love.graphics.clear)
  (_G.rs.start)
  (_G.assets.background:draw 0 0)
  (let [draw-outlines? _G.debug-mode]
   (_G.world:draw nil draw-outlines?))
  (when _G.debug-mode
    (love.graphics.print (love.timer.getFPS) 0 0))
  (_G.rs.stop)
  ;; (love.graphics.print (inspect _G.world.colliders {:process
  ;;                                                   ;; #$
  ;;                                                   #(if (not= (type $) :function) $)
  ;;                                                   })
  ;;                        20 20 {:r 0 :g 0 :b 0})
  (when _G.debug-mode
    (let [(w h) (love.graphics.getDimensions)]
      (draw-axis-guides w h))))

(fn love.update [dt]
  (_G.Timer.update dt))

(fn love.keypressed [key]
  (match key
    :f1 (print (inspect _G.world._world))
    :escape (love.event.quit)
    :f4 (let [(w h {:borderless borderless?}) (love.window.getMode)]
          (love.window.setMode w h {:borderless (not borderless?)}))
    :f11 (let [(fullscreen? _) (love.window.getFullscreen)]
           (love.window.setFullscreen (not fullscreen?)))))

(_G.Gamestate.registerEvents)
