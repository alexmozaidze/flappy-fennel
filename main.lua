fennel = require "fennel"
debug.traceback = fennel.traceback
table.insert(package.loaders or package.searchers, fennel.searcher)

table.unpack = rawget(table, "unpack") or unpack

-- Defining globals
inspect = require "lib/inspect/inspect"
function math.clamp(low, n, high)
	return math.min(math.max(low, n), high)
end

-- Debug mode
do
	local debug_mode = os.getenv "DEBUG_MODE"
	_G["debug-mode"] = debug_mode == "1" or debug_mode == "true"
end

-- Global Constants
_G["canvas"] = { width = 250, height = 256 }
_G["screen"] = { width = canvas.width * 3, height = canvas.height * 3 }
_G["physics-meter"] = 64
_G["ground-height"] = canvas.height - 40

fennel.dofile("main.fnl", { requireAsInclude = not _G["debug-mode"] })
