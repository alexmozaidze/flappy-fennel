# Flappy Fennel

Flappy Bird clone made using [Love2D](https://love2d.org/) and [Fennel](https://fennel-lang.org/).

The codebase is extremely disorganized, and many assets remain unused.

![Flappy Fennel Screenshot](flappy-fennel-screenshot.png)

## Running

```sh
git clone --recurse-submodules https://gitlab.com/alexmozaidze/flappy-fennel.git
cd flappy-fennel
love .
```
